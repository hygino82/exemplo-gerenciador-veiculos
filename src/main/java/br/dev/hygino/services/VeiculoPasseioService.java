package br.dev.hygino.services;

import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.dev.hygino.dto.InsertVeiculoPasseioDTO;
import br.dev.hygino.dto.VeiculoPasseioDTO;
import br.dev.hygino.entities.VeiculoPasseio;
import br.dev.hygino.repositories.VeiculoPasseioRespository;
import jakarta.validation.Valid;

@Service
public class VeiculoPasseioService {

    private final VeiculoPasseioRespository veiculoPasseioRespository;

    public VeiculoPasseioService(VeiculoPasseioRespository veiculoPasseioRespository) {
        this.veiculoPasseioRespository = veiculoPasseioRespository;
    }

    @Transactional
    public VeiculoPasseioDTO insert(@Valid InsertVeiculoPasseioDTO dto) {
        VeiculoPasseio veiculoPasseioEntity = new VeiculoPasseio();
        copyDtoToEntity(veiculoPasseioEntity, dto);
        veiculoPasseioEntity = veiculoPasseioRespository.save(veiculoPasseioEntity);
        return new VeiculoPasseioDTO(veiculoPasseioEntity);
    }

    private void copyDtoToEntity(VeiculoPasseio entity, @Valid InsertVeiculoPasseioDTO dto) {
        entity.setQuantidadePassageiros(dto.quantidadePassageiros());
        entity.setPotencia(dto.potencia());
        entity.setQuantidadePistoes(dto.quantidadePistoes());
        entity.setQuantidadeRodas(dto.quantidadeRodas());
        entity.setVelocidadeMaxima(dto.velocidadeMaxima());
        entity.setCor(dto.cor());
        entity.setModelo(dto.modelo());
        entity.setMarca(dto.marca());
        entity.setPlaca(dto.placa());
    }

    @Transactional(readOnly = true)
    public List<VeiculoPasseioDTO> findAll() {
        List<VeiculoPasseio> list = veiculoPasseioRespository.findAll();
        return list.stream().map(x -> new VeiculoPasseioDTO(x)).collect(Collectors.toList());
    }
}
