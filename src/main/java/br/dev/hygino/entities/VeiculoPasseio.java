package br.dev.hygino.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(of = "id")
@Entity
@Table(name = "tb_passeio")
public class VeiculoPasseio {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    private String placa;

    @NotBlank
    private String marca;

    @NotBlank
    private String modelo;
    
    @NotBlank
    private String cor;
    
    @NotNull
    private Float velocidadeMaxima;

    @NotNull
    private Short quantidadeRodas;

    @NotNull
    private Short quantidadePistoes;

    @NotNull
    private Short potencia;

    @NotNull
    private Short quantidadePassageiros;
}
