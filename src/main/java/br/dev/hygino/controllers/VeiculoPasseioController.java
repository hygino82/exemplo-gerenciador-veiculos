package br.dev.hygino.controllers;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.dev.hygino.dto.InsertVeiculoPasseioDTO;
import br.dev.hygino.dto.VeiculoPasseioDTO;
import br.dev.hygino.services.VeiculoPasseioService;
import jakarta.validation.Valid;

@RestController
@RequestMapping("api/v1/passeio")
public class VeiculoPasseioController {

    private final VeiculoPasseioService veiculoPasseioService;

    public VeiculoPasseioController(VeiculoPasseioService veiculoPasseioService) {
        this.veiculoPasseioService = veiculoPasseioService;
    }

    @PostMapping
    public ResponseEntity<VeiculoPasseioDTO> insert(@RequestBody @Valid InsertVeiculoPasseioDTO dto) {
        VeiculoPasseioDTO res = veiculoPasseioService.insert(dto);
        return ResponseEntity.status(201).body(res);
    }

    @GetMapping
    public ResponseEntity<List<VeiculoPasseioDTO>> findAll() {
        List<VeiculoPasseioDTO> res = veiculoPasseioService.findAll();
        return ResponseEntity.status(200).body(res);
    }
}
