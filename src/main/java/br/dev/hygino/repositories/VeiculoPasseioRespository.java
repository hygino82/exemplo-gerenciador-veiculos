package br.dev.hygino.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.dev.hygino.entities.VeiculoPasseio;

@Repository
public interface VeiculoPasseioRespository extends JpaRepository<VeiculoPasseio, Long> {

}
