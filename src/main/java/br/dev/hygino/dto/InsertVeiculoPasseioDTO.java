package br.dev.hygino.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public record InsertVeiculoPasseioDTO(
                @NotBlank String placa,
                @NotBlank String marca,
                @NotBlank String modelo,
                @NotBlank String cor,
                @NotNull Float velocidadeMaxima,
                @NotNull Short quantidadeRodas,
                @NotNull Short quantidadePistoes,
                @NotNull Short potencia,
                @NotNull Short quantidadePassageiros) {
}
