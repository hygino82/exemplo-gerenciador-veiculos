package br.dev.hygino.dto;

import br.dev.hygino.entities.VeiculoPasseio;

public record VeiculoPasseioDTO(
                Long id,
                String placa,
                String marca,
                String modelo,
                String cor,
                Float velocidadeMaxima,
                Short quantidadeRodas,
                Short quantidadePistoes,
                Short potencia,
                Short quantidadePassageiros) {

        public VeiculoPasseioDTO(VeiculoPasseio obj) {
                this(
                                obj.getId(),
                                obj.getPlaca(),
                                obj.getMarca(),
                                obj.getModelo(),
                                obj.getCor(),
                                obj.getVelocidadeMaxima(),
                                obj.getQuantidadeRodas(),
                                obj.getQuantidadePistoes(),
                                obj.getPotencia(),
                                obj.getQuantidadePassageiros());
        }
}
